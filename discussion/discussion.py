# Input
# input() allow us to gather data from the user input, returns string data type
# \n - line break

# username = input("Please enter your name: \n")
# print(f"Hi {username}! Welcome to Python Short Course")

# num1 = input("Enter 1st number: \n")
# num2 = input("Enter 2nd number: \n")
# print(f"The sum of num1 and num2 is {num1 + num2}")

# num1 = int(input("Enter 1st number: \n"))
# num2 = int(input("Enter 2nd number: \n"))
# print(f"The sum of num1 and num2 is {num1 + num2}")

# If-else statemenrs - used to choose between two or more code blocks depending on the condition

# Declare a variable to use for the conditional statement
# test_num = 75

# if test_num >= 60:
# 	print("Test Passed.")
# else:
# 	print("Test Failed.")

# Else-if chains 
# test_num2 = int(input("Please enter the 2nd test number: \n"))

# if test_num2 > 0:
# 	print("The number is positive")
# elif test_num2 == 0:
# 	print("The number is zero")
# else:
# 	print("The number is negative")	

# Mini-Exercise:
# Create an if-else statement that determines if a number is divisible by 3, 5, or both.
# If the number is divisible by 3, print "The number is divisible by 3"
# If the number is divisible by 5, print "The number is divisible by 5"
# If the number is divisible by 5 and 3, print "The number is divisible by both 3 and 5"
# If the number is not divisible by any, print "The number is not divisible by 3 nor 5"

# test_num3 = int(input("Please enter the 3rd test number: \n"))

# if test_num3 % 3 == 0 and test_num3 % 5 == 0:
# 	print("The number is divisible by both 3 and 5")
# elif test_num3 % 3 == 0:
# 	print("The number is divisible by 3")
# elif test_num3 % 5 == 0:
# 	print("The number is divisible by 5")
# else:
# 	print("The number is not divisible by 3 nor 5")	

# Loops

# i = 1
# while i <= 5:
# 	print(f"Current Count {i}")
# 	i += 1

# For Loops are used for iterating over a sequence
# fruits = ["apple", "banana", "cherry"]
# for indiv_fruit in fruits:
# 	print(indiv_fruit)

# range() - iterate through values
# range() - defaults 0 to start value
# Syntax - range(stop)
# for x in range(6,10,2):
# 	print(f"The current value is {x}")

# Break - stops loop
# j = 1
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j += 1

# Continue - returns control to beginning of while loop and continue to next iteration
# k = 1
# while k < 6:
# 	k += 1
# 	if k == 3:
# 		continue
# 	print(k)