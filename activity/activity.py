year = input("Please input a year: \n")

if year.isdigit() == False or int(year) <= 0:
	print("Year cannot be a zero, a negative value, or a string.")
elif int(year) % 4 == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")

row = int(input("Enter number of rows: \n"))
col = int(input("Enter number of columns: \n"))

for x in range(row):
	for y in range(col):
		print("*", end = '')
	print()